let bonusesInfo = document.querySelector('.bonuses__info')
let bonusesBtn = document.querySelector('.bonuses__more .show-all')

bonusesBtn.addEventListener('click', () => {
    bonusesInfo.classList.toggle('toggle-list');
    bonusesBtn.classList.toggle('toggle-arrow');
});

// Open-close FAQ
let details = document.querySelectorAll("details");
for(let i = 0; i < details.length; i++) {
    details[i].addEventListener("toggle", accordion);
}
function accordion(event) {
    if (!event.target.open) return;
    let details = event.target.parentNode.children;
    for(let i = 0; i < details.length; i++) {
        if (details[i].tagName !== "DETAILS" ||
            !details[i].hasAttribute('open') ||
            event.target === details[i]) {
            continue;
        }
        details[i].removeAttribute("open");
    }
}